<?php 
//$page = 'sitemap';
$pg = ['property' => 'freudenberg', 'page' => 'sitemap'];
include 'includes/header.php'; 
?>

    <style>
        #site_m_t a:link,#site_m_t a:visited{
            font-family: 'pt_sansregular', Arial, Helvetica, sans-serif;
            color: #0073BC;
            text-decoration:none;
        }
        #site_m_t a:hover{
            color:#05446B;
        }
        #site_m_t a{font-size:15px;}

        #site_m_t td{
            width:43%;
            vertical-align: top;
        }

        .site_map p{font-size:9.5pt;line-height:2em}.site_map td, .site_map th{font-size:9.2pt;line-height:1.5em}
        .site_map h2.label {
            font-family: pt_sansregular,Arial,Helvetica,sans-serif;
            font-size: 11pt;
            color: #0073BC;
            text-transform: uppercase;
            margin-bottom: 15px;
            font-weight: 400;
        }
        #site_m_t ul, .fac_tr ul {
            padding: 0;
            margin: 20px 20px;
        }
        .hdr-two {
            text-align:left !important;font-size: 35px;
        }
        .ctatext-wrapper {
            padding-bottom: 0%;
        }
        .ctatext-text {
            width: 55%;
        }
        @media screen and (max-width:768px){
            .ctatext-wrapper {
                padding-top:2%;
                padding-bottom: 0%;
            }
            .ctatext-text {
                width: 80%;
            }
        }
        @media screen and (max-width:420px){
            #site_m_t td{
                width:80%;
                float:left;
            }
            table td {
                padding: 0px;
            }	
        }
    </style>

    <body>

        <header id="header" role="banner">

            <?php include 'includes/navigation.php'; ?> 
        </header><!--  #header  -->
        <?php include 'includes/booking.php'; ?> 

        <div class="blur">  
            <div class="node--page_basic mode--full">    
                <aside role="complementary">
                    <?php include 'includes/_slider.php'; ?>
                </aside>   
                <div id="route" style="margin-left: 21.4% !important;">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Site Map</li>
                    </breadcrumb>
                </div>
                <div id="site_m_t" class="site_map">
                    <div class="ctatext-wrapper">
                        <div class="ctatext-text pad_top">
                            <div class="hdr-two">Site Map</div>
                        </div>
                    </div>
                    <table>
                        <tr>
                            <td>
                                <h2 class="label">Hotels</h2>
                                <ul>
                                    <li><a title="Randholee Resorts" href="<?php echo HTTP_PATH ?>hotels/randholee/index.php" >Randholee Luxury Resorts</a></li>
                                    <li><a title="The Firs" href="<?php echo HTTP_PATH ?>hotels/firs/index.php" >The Firs</a></li>
                                    <li><a title="Ellen's Place" href="<?php echo HTTP_PATH ?>hotels/ellens/index.php">Ellen's Place</a></li>
                                </ul>

                                <h2 class="label"><a target="_blank" href="accommodation.php">Accommodation</a></h2>
                                <h2 class="label"><a target="_blank" href="experience.php">Experiences</a></h2>                          
                                <h2 class="label"><a target="_blank" href="promotions.php">Special Offers</a></h2>

                            </td>

                            <td>
                                <h2 class="label">INQUIRIES</h2>
                                <ul>
                                    <li><a target="_blank" href="about-us.php">About Us</a></li>
                                    <li><a target="_blank" href="contact-us.php">Contact Us</a></li>
                                </ul>
                            </td>

                            <td>
                                <h2 class="label">CONNECT</h2>
                                <ul>
                                    <li><a target="_blank" href="https://www.facebook.com/Freudenberg.Leisure/?ref=bookmarks" title="Freudenberg Leisure Facebook">Facebook</a></li>
                                    <li><a target="_blank" href="https://plus.google.com/u/0/108199145368696310148/photos" title="Freudenberg Leisure G+">Google+</a></li>
                                    <li><a target="_blank" href="https://www.youtube.com/channel/UCESdt6RC1D0m8EhcBaLvcKQ" title="Freudenberg Leisure Youtube">Youtube</a></li>
                                    <li><a target="_blank" href="https://www.flickr.com/photos/136744566@N04/" title="Freudenberg Leisure Flickr">Flickr</a></li>
                                </ul>

                            </td>
                        </tr>
                    </table>
                </div>


            </div><!--  #node-details  -->

            <div style="clear:both"></div>.

            <footer id="footer" role="contentinfo">    

                <?php /* ?> <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>


                <?php include 'includes/footer.php'; ?>

                </body>
                </html>