<div class="tab">
  <button class="tablinks active" onclick="openTab(event, 'Description')">Page Description</button>
  <button class="tablinks" onclick="openTab(event, 'ImgPanel')">Navigation Panel</button>
  <button class="tablinks" onclick="openTab(event, 'Properties')">Properties</button>
</div>

<div id="Description" class="tabcontent">
  <h1>Edit <?=$pagedata->page_name?> Page Description</h1>
  <form id="desc">
  	<table>
  		<tr>
  			<td><label for="subtitle">Sub Title</label></td>
  			<td><input type="text" name="subtitle" id="subtitle" value=""/></td>
  		</tr>
  		<tr>
   			<td><label for="maintitle">Main Title</label></td>
  			<td><input type="text" name="maintitle" id="maintitle" value=""/></td> 		
  		</tr>
  		<tr>
   			<td><label for="bodytext">Body Text</label></td>
  			<td><textarea name="bodytext" id="bodytext" rows="10" cols="75"></textarea></td> 		
  		</tr> 
  		<tr>
  			<td></td>
  			<td><button type="button" id="updbtn" name="updbtn">Update</button></td>
  		</tr> 		
  	</table>
  </form>
</div>

<div id="ImgPanel" class="tabcontent" style="height: 100%;">
  <h1>Manage <?=$pagedata->page_name?> Page Navigation</h1>
  <div class="gridcontainer" id="dropzone">
  	<div class="mainbox">
  		<form action="/file-upload" class="dropzone" id="mainimage-dropzone" style="height: 316px;"></form>
  	</div>
  	<div class="otherboxes">
  		<div class="smallbox" style="width: 33%; float: left; margin-left: 5px;">
  			<form action="/file-upload" class="dropzone" id="smallimage-dropzone-1"></form>
  		</div>
  		<div class="smallbox" style="width: 33%; float: left; margin-left: 5px;">
  			<form action="/file-upload" class="dropzone" id="smallimage-dropzone-2"></form>
  		</div>
  		<div class="smallbox" style="clear: both; width: 33%; float: left; margin-left: 5px;">
  			<form action="/file-upload" class="dropzone" id="smallimage-dropzone-3"></form>
  		</div>
  		<div class="smallbox" style="width: 33%; float: left; margin-left: 5px;">
  			<form action="/file-upload" class="dropzone" id="smallimage-dropzone-4"></form>
  		</div>
  	</div>
  </div>
</div>

<div id="Properties" class="tabcontent" style="height: 100%">
  <h1>Manage <?=$pagedata->page_name?> Page Featured Properties</h1>
  <div class="gridcontainer">
  	<div class="" style="">
  		<!-- property 1 -->
  		<form action="/file-upload" class="dropzone" id="property1-dropzone-1" style="float: left; height: 200px; width: 200px;"></form>
  		<div class="property1-desc" style="margin-left: 10px; float: left;">
  			<form>
  				<div class="field">
  					<label for="property1-title">Title</label><br>
   					<input type="text" name="property1-title" id="property1-title" value=""/>
   				</div>
   				<div class="field" style="margin-top: 20px;">
  					<label for="property1-desc">Description</label><br>
  					<textarea name="property1-desc" id="property1-desc" rows="5" cols="75"></textarea>
  				</div>
  			</form>
  		</div>
  	</div>
  	<div class="" style="clear: left;">
  		<!-- property 2 -->
  		<form action="/file-upload" class="dropzone" id="property2-dropzone-1" style="float: left; height: 200px; width: 200px;"></form>
  		 <div class="property2-desc" style="margin-left: 10px; float: left;">
  			<form>
  				<div class="field">
  					<label for="property1-title">Title</label><br>
   					<input type="text" name="property1-title" id="property1-title" value=""/>
   				</div>
   				<div class="field" style="margin-top: 20px;">
  					<label for="property1-desc">Description</label><br>
  					<textarea name="property1-desc" id="property1-desc" rows="5" cols="75"></textarea>
  				</div>
  			</form>  		 
  		</div>
  	</div>
  	<div class="" style="clear: left;">
  		<!-- property 3 -->
  		<form action="/file-upload" class="dropzone" id="property3-dropzone-1" style="float: left; height: 200px; width: 200px;"></form>
  		<div class="property3-desc" style="margin-left: 10px; float: left;">
  			<form>
  				<div class="field">
  					<label for="property1-title">Title</label><br>
   					<input type="text" name="property1-title" id="property1-title" value=""/>
   				</div>
   				<div class="field" style="margin-top: 20px;">
  					<label for="property1-desc">Description</label><br>
  					<textarea name="property1-desc" id="property1-desc" rows="5" cols="75"></textarea>
  				</div>
  			</form>  		
  		</div>
  	</div>
  </div>
</div>