<section class="content-header">
<?php if($pagedata->show_tabs == 1): ?>
<p style="color: red;">Please click on a tab to view its contents.</p>
<div class="tab">
  <?php if($pagedata->section_desc == 1): ?>
    <button class="tablinks" onclick="openTab(event, 'Description', this)" data-extra="">Description</button>
  <?php endif; ?>
  <?php if($pagedata->section_nav == 1): ?>
    <button class="tablinks" onclick="openTab(event, 'Navigation', this)" data-extra="">Navigation Panel</button>
  <?php endif; ?>
  <?php if($pagedata->section_gallery == 1): ?>
    <button class="tablinks" onclick="openTab(event, 'Gallery', this)" data-extra="gallery">Page Gallery</button>
  <?php endif; ?>
  <?php if($pagedata->section_slider == 2): ?>
    <button class="tablinks" onclick="openTab(event, 'Gallery', this)" data-extra="slider">Page Slider</button>
  <?php endif; ?>
  <?php if($pagedata->id == 1): ?>
    <button class="tablinks" onclick="openTab(event, 'Properties', this)" data-extra="">Properties</button>
  <?php endif; ?>
  <?php if($pagedata->id == 4): ?>
    <button class="tablinks" onclick="openTab(event, 'Experiences', this)" data-extra="">Experiences</button>
  <?php endif; ?>
  <?php if($pagedata->allow_subpages == 1): ?>    
    <button class="tablinks" style="float: right; color: white; background: green;" onclick="window.location = '<?=SITE_URL.'/manage/'.$property_slug.'/addpage/'.urlencode(base64_encode($pageid))?>'">+ Add Sub Page</button>
  <?php endif; ?>
</div>
<?php endif; ?>
</section>

<section class="content">
<?php if($pagedata->section_desc == 1): ?>
<div id="Description" class="tabcontent">
  <h1>Edit <?php echo (isset($parentpg->page_name) ? $parentpg->page_name . ' -> ' : '') . $pagedata->page_name; ?> Page Description &nbsp;&nbsp;<span id="actionfeedback" style="vertical-align: middle; font-size: 20px; font-weight: 600;"></span></h1>
  <form role="form" id="frmpgdesc" action="<?=SITE_URL.'/updatedesc.php'?>">
  	 <div class="form-group">
  	 	<label for="subtitle">Sub Title</label><br>
  	 	<input type="text" class="form-control" name="subtitle" id="subtitle" value=""/>
  	 </div>
  	 <div class="form-group">
  	 	<label for="maintitle">Main Title <span class="requiredmsg">(Required)</span></label><br>
  	 	<input type="text" class="form-control" name="maintitle" id="maintitle" value=""/>
  	 </div>
  	 <div class="form-group">
  	 	<label for="bodytext">Body Text <span class="requiredmsg">(Required)</span></label><br>
  	 	<textarea name="bodytext" id="bodytext" rows="15" cols="75" class="form-control"></textarea>
  	 </div>
  	 <div class="form-group">
  	 	<input type="hidden" name="desc-id" value=""/>
  	 	<input type="hidden" name="desc-page-id" value=""/>
  	 	<input type="submit" class="btn btn-primary" id="submit" name="submit"/>
  	 </div>
  </form>
</div>
<?php endif; ?>

<?php if($pagedata->section_nav == 1): ?>
<div id="Navigation" class="tabcontent" style="/*height: 100%;*/">
  <h1>Manage <?=$pagedata->page_name?> Page Navigation</h1>

  <div class="navlayout" style="/*height: 100%;*/padding-top: 25px;">
	 	<div class="3imagenav">
	 		<div class="" style="border: 1px dotted red; padding: 15px; background: rgba(197, 197, 2, 0.23);">
	 			<p style="margin: 0px;"><a href="#" id="popme"><i class="icon fa fa-plus-square"></i> Add New Item</a></p>
	 		</div>
		</div>
		<div class="nav-item-list-container">
			<h3>Navigation Items</h3>
			<div class="nav-item-list">
			</div>
		</div>
  </div>
</div>

<!-- model -->
<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="height: 450px; padding: 20px;">
        <form role="form" id="frmnavigation" method="post" action="<?=SITE_URL.'/ajax.php'?>" enctype="multipart/form-data">
              <div style="float: left; width: 500px;">
              <div class="form-group" style="">
                <label for="navitem-name">Title <span class="requiredmsg">(Required)</span></label><br>
                <input type="text" class="form-control" name="navitem-name" id="navitem-name" value=""/>
              </div>
              <!--                    
              <div class="field" style="margin-top: 10px; clear: left;">
                <label for="navitem-tagline">Tagline <span class="requiredmsg">(Required)</span></label><br>
                <input type="text" style="width: 374px;" name="navitem-tagline" id="navitem-tagline" value=""/>
              </div>-->
                
              <div class="form-group" style="margin-top: 10px;">
                <label for="navitem-desc">Description <span class="requiredmsg">(Required)</span></label><br>
                <textarea class="form-control" name="navitem-desc" id="navitem-desc" rows="5" cols="50"></textarea>
              </div>

              <div class="form-group" style="margin-top: 10px;">
                <div style="width: 45%; float: left; margin-right: 50px;">
                  <label for="navitem-target-pg-property">Property <span class="requiredmsg">(Required)</span></label>
                  <select name="navitem-target-pg-property" class="form-control">
                    <option value="">Select Property</option>                  
                  </select>
                </div>
                <div style="width: 45%; float: left;">
                  <label for="navitem-target">Target Page <span class="requiredmsg">(Required)</span></label>
                  <select name="navitem-target" class="form-control">
                    <option value="">Select a Property First ...</option>
                  </select>
                </div>
                <br style="clear: left;">
              </div>
              
              <div class="form-group" style="">
                <label for="navitem-image">Select Image <span class="requiredmsg">(Required)</span></label><br>
                <input type="file" id="navitem-image" name="navitem-image" data-preview-element="nav-image-preview" onchange="preview_image(this);"/>
                <p class="help-block">Example block-level help text here.</p>
              </div>
              <div class="field hiddenstuff" style="margin-top: 20px;">
                <input type="hidden" name="navitem-page-id" value="<?=$pagedata->id?>"/>
                <input type="hidden" name="navitem-action" value=""/>
                <!--<button type="button" id="navitemupload" name="navitemupload">Add Item</button>-->
                <input type="submit" name="navitemsave" id="navitemsave" value="Add Item!" class="btn btn-primary"/>
              </div>
          </div>
          <div class="nav-image-preview-area" style="float: right; width: 265px; height: 336px; overflow: hidden; margin-right: 70px; margin-top: 6px; text-align: center; border-left: 1px solid darkgrey; padding-left: 10px;">
            <h3>Image Preview</h3>
            <div id="nav-image-preview">
              <p>Image not selected!</p>
            </div>
          </div>
        </form>
    </div>
  </div>
</div>
<?php endif; ?>

<?php if($pagedata->section_gallery == 1 || $pagedata->section_slider == 2): ?>
<div id="Gallery" class="<?= ($pagedata->show_tabs == 0 ? 'singletabdisplay' : 'tabcontent') ?>" style="position: relative;" data-extra="<?= ($pagedata->section_gallery == 1 ? 'gallery' : 'slider') ?>">
  <h1>Manage <?php echo (in_array($pagedata->id, array(27,28,33,34,35,42,37,38)) ? $propertydata->property_name : (isset($parentpg->page_name) ? $parentpg->page_name . ' -> ' .$pagedata->page_name : $pagedata->page_name))?> <?php echo ($pagedata->section_gallery == 1 ? 'Image Gallery' : ($pagedata->section_slider ? 'Slider Images' : '')) ?></h1>
  <!--uploadify-->
  <!--<div id="fileQueue" style="width: 328px; height: 100%; position: absolute; top: 0px; right: 10px; border: none;"></div>-->
  <div style="border: 1px dotted red; padding: 15px; padding-top: 12px; background: rgba(197, 197, 2, 0.23); height: 114px;">
    <div style="float: left;" id="uploadify-container">
      <p><a href="javascript:jQuery('#uploadify').uploadifyClearQueue()">Cancel All Uploads</a></p>
    </div>
    <div id="fileQueue" style="float: right; height: 78px; overflow: hidden; padding-left: 10px; border: none;">
      <span id="QueueUploadStatus1"></span>
      <span id="QueueUploadStatus">Uploaded <span id="fileQueueRunningCounter"></span> out of <span id="fileQueueTotal"></span> Files</span><br>
    </div>
  </div>
  <div class="slider-image-list" style="margin-top: 30px; display: inline-block;">
  </div>
</div>
<?php endif; ?>


<?php if($pagedata->id == 1): ?>
  <div id="Properties" class="tabcontent">
    <!--<p>Under Development</p>-->
    <h1>Manage <?=$pagedata->page_name?> Properties</h1>

    <div class="proplayout" style="/*height: 100%;*/padding-top: 25px;">
      <div class="3imageexp">
        <div class="" style="border: 1px dotted red; padding: 15px; background: rgba(197, 197, 2, 0.23);">
          <p><a href="#" id="poppropmodel">+ Add New Item</a></p>
        </div>
      </div>
      <div class="prop-item-list-container">
        <h3>Properties</h3>
        <div class="prop-item-list">
        </div>
      </div>
    </div>
  </div>

  <!-- Model -->
  <div id="myPropertiesModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="height: 500px; padding: 16px;">
          <form role="form" id="frmproperties" method="post" action="<?=SITE_URL.'/ajax-properties.php'?>" enctype="multipart/form-data">
            <div style="float: left; width: 500px; border-right: 1px solid darkgrey; padding-right: 28px;">
                <h3 style="margin-top: 0px;">Property Item</h3>

                <div class="form-group" style="margin-top: 10px;">
                  <label for="propitem-name">Property Name <span class="requiredmsg">(Required)</span></label><br>
                  <input type="text" class="form-control" style="/*width: 374px;*/" name="propitem-name" id="propitem-name" value=""/>
                </div>

                <div class="form-group" style="margin-top: 10px;">
                  <label for="propitem-tlink">Target Link <span class="requiredmsg">(Required)</span></label><br>
                  <input type="text" class="form-control" style="/*width: 374px;*/" name="propitem-tlink" id="propitem-tlink" value=""/>
                </div>                
                  
                <div class="form-group" style="margin-top: 10px;">
                  <label for="propitem-desc">Property Description <span class="requiredmsg">(Required)</span></label><br>
                  <textarea name="propitem-desc" id="propitem-desc" rows="5" cols="60" class="form-control" style="/*width: 374px;*/"></textarea>
                </div>         

                
                <div class="form-group" style="">
                  <div class="fg-left" style="float: left; width: 45%;">
                    <label for="propitem-image">Select Property Image <span class="requiredmsg">(Required)</span></label><br>
                    <input type="file" id="propitem-image" name="propitem-image" data-preview-element="prop-image-preview" onchange="preview_image(this);"/>
                    <p class="help-block">Example block-level help text here.</p>                      
                  </div>
                  <div class="fg-right" style="width: 45%; margin-left: 55%;">
                    <label for="propitem-browsertab">Open With</label><br>
                    <select name="propitem-browsertab" class="form-control">
                      <option value="_self">Current Window</option>
                      <option value="_blank">A New Tab</option>
                    </select>
                  </div>
                </div>             

                <div class="form-group prophiddenstuff" style="margin-top: 50px; clear: left;">
                  <input type="hidden" name="propitem-page-id" value="<?=$pagedata->id?>"/>
                  <input type="hidden" name="propitem-action" value=""/>
                  <!--<button type="button" id="navitemupload" name="navitemupload">Add Item</button>-->
                  <input type="submit" name="propitemsave" id="propitemsave" value="Add Item!" class="btn btn-primary"/>
                </div>
            </div>
            <div class="prop-image-preview-area" style="float: right; width: 265px; height: 336px; overflow: hidden; margin-right: 70px; margin-top: 6px; text-align: center; /*border-left: 1px solid darkgrey;*/ padding-left: 10px;">
              <h3>Image Preview</h3>
              <div id="prop-image-preview">
                <p>Image not selected!</p>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>     
<?php endif; ?>

<?php if($pagedata->id == 4): ?>
  <div id="Experiences" class="tabcontent">
    <!--<p>Under Development</p>-->
    <h1>Manage <?=$pagedata->page_name?> Experiences</h1>

    <div class="explayout" style="/*height: 100%;*/padding-top: 25px;">
      <div class="3imageexp">
        <div class="" style="border: 1px dotted red; padding: 15px; background: rgba(197, 197, 2, 0.23);">
          <p><a href="#" id="popexpmodel">+ Add New Item</a></p>
        </div>
      </div>
      <div class="exp-item-list-container">
        <h3>Experience Items</h3>
        <div class="exp-item-list">
        </div>
      </div>
    </div>    
  </div>

  <!-- Model -->
  <div id="myExperienceModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="height: 450px; padding: 15px;">
          <form role="form" id="frmexperience" method="post" action="<?=SITE_URL.'/ajax-experience.php'?>" enctype="multipart/form-data">
                <div style="float: left; width: 500px;">
                <div class="form-group" style="margin-top: 10px;">
                  <label for="expitem-destination">Destination <span class="requiredmsg">(Required)</span></label><br>
                  <select name="expitem-destination" class="form-control">
                    <option value="">Select Destination</option>
                    <option value="1">Colombo</option>
                    <option value="2">Kandy</option>
                    <option value="2">Nuwara Eliya</option>
                  </select>
                </div>                    
                <div class="form-group" style="margin-top: 10px; clear: left;">
                  <label for="expitem-tagline">Tagline <span class="requiredmsg">(Required)</span></label><br>
                  <input type="text" style="/*width: 374px;*/" name="expitem-tagline" id="expitem-tagline" value="" class="form-control"/>
                </div>
                  
                <div class="form-group" style="margin-top: 10px;">
                  <label for="expitem-desc">Description <span class="requiredmsg">(Required)</span></label><br>
                  <textarea name="expitem-desc" id="expitem-desc" rows="5" cols="50" class="form-control"></textarea>
                </div>
                <div class="form-group" style="">
                  <label for="expitem-image">Select Image <span class="requiredmsg">(Required)</span></label><br>
                  <input type="file" id="expitem-image" name="expitem-image" data-preview-element="exp-image-preview" onchange="preview_image(this);"/>
                  <p class="help-block">Example block-level help text here.</p>
                </div>
                <div class="form-group exphiddenstuff" style="margin-top: 20px;">
                  <input type="hidden" name="expitem-page-id" value="<?=$pagedata->id?>"/>
                  <input type="hidden" name="expitem-action" value=""/>
                  <!--<button type="button" id="navitemupload" name="navitemupload">Add Item</button>-->
                  <input type="submit" name="expitemsave" id="expitemsave" value="Add Item!" class="btn btn-primary"/>
                </div>
            </div>
            <div class="exp-image-preview-area" style="float: right; width: 265px; height: 336px; overflow: hidden; margin-right: 70px; margin-top: 6px; text-align: center; border-left: 1px solid darkgrey; padding-left: 10px;">
              <h3>Image Preview</h3>
              <div id="exp-image-preview">
                <p>Image not selected!</p>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>     
<?php endif; ?>

<?php if($pagedata->id == 29): ?>
  <div id="Promotions" class="<?= ($pagedata->show_tabs == 0 ? 'singletabdisplay' : 'tabcontent') ?>" style="position: relative;" data-extra="<?=$propertydata->id?>">
      <h1>Manage <?=$propertydata->property_name?> Promotions</h1>
      <div class="promolayout" style="/*height: 100%;*/padding-top: 25px;">
        <div class="3imageexp">
          <div class="" style="border: 1px dotted red; padding: 15px; background: rgba(197, 197, 2, 0.23);">
            <p style="float: left;"><a href="#" id="poppromomodel">+ Add New Promotion</a></p>
            <?php if($propertydata->id == 1): ?>
            <div class="promo-list-filter-controls" style="float: right;">
              <label for="property-selector">Select Property</label>
              <select name="property-selector" id="property-selector">
                <option value="1" <?=($propid == 1 ? 'selected="selected"' : '')?>>All Properties</option>
                <option value="2" <?=($propid == 2 ? 'selected="selected"' : '')?>>Randholee Resorts &amp; Spa</option>
                <option value="3" <?=($propid == 3 ? 'selected="selected"' : '')?>>The Firs</option>
                <option value="4" <?=($propid == 4 ? 'selected="selected"' : '')?>>Ellens Place</option>
              </select>
            </div>
            <?php endif; ?>
            <br style="clear: both;">            
          </div>
        </div>
        <div class="promo-item-list-container">
          <div class="promo-item-list"></div>
        </div>
      </div>
  </div>

  <!-- Model -->
  <div id="myPromoModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myPromoModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="height: 550px; padding: 10px;">
          <form id="frmpromotion" method="post" action="<?=SITE_URL.'/ajax-promotionsdd.php'?>" enctype="multipart/form-data">
              <div id="promo-details" style="float: left; width: 500px;">
                <h3 style="margin-top: 0px;">Promotion Item</h3>
                <div class="form-group" style="margin-top: 10px;">
                  <label for="promoitem-property">Property <span class="requiredmsg">(Required)</span></label><br>
                  <select name="promoitem-property" class="form-control"></select>
                </div>                    
                <div class="form-group" style="margin-top: 10px; clear: left;">
                  <label for="promoitem-title">Title <span class="requiredmsg">(Required)</span></label><br>
                  <input type="text" class="form-control" name="promoitem-title" id="promoitem-title" value=""/>
                </div>
                  
                <div class="form-group" style="margin-top: 10px;">
                  <label for="promoitem-desc">Description <span class="requiredmsg">(Required)</span></label><br>
                  <textarea class="form-control" name="promoitem-desc" id="promoitem-desc" rows="5" cols="50"></textarea>
                </div>
                <div class="form-group">
                  <div class="form-group" style="float: left;">
                    <label>Promotion click action</label>
                    <div class="radio">
                      <label>
                        <input type="radio" name="promoitem-trigger" id="trigger1" value="none" checked>
                        None
                      </label>
                    </div>                  
                    <div class="radio">
                      <label>
                        <input type="radio" name="promoitem-trigger" id="trigger1" value="page">
                        Open a page
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="promoitem-trigger" id="trigger2" value="image">
                        Popup an image
                      </label>
                    </div>
                  </div>
                  <div class="form-group" style="margin-left: 200px; padding-top: 10px;">
                    <label for="promoitem-image">Select Image <span class="requiredmsg">(Required)</span></label><br>
                    <input type="file" id="promoitem-image" name="promoitem-image" data-preview-element="promo-image-preview" onchange="preview_image(this);"/>
                    <p class="help-block">Example block-level help text here.</p>
                  </div>              
                </div>
                <div class="field promohiddenstuff" style="clear: left;">
                  <input type="hidden" name="promoitem-page-id" value="<?=$pagedata->id?>"/>
                  <input type="hidden" name="promoitem-action" value=""/>
                  <input type="checkbox" name="promoitem-enabled" id="promoitem-enabled" value="true"/>&nbsp;<label for="promoitem-enabled">Enabled</label>
                  <br>
                  <br>
                  <input type="submit" name="promoitemsave" id="propitemsave" value="Add Item!" class="btn btn-primary"/>                
                </div>
              </div>
              <div class="promo-image-preview-area" style="float: right; width: 265px; height: 336px; overflow: hidden; margin-right: 70px; margin-top: 6px; /*text-align: center;*/ border-left: 1px solid darkgrey; padding-left: 10px;">
                <!--<h3>Image Preview</h3>-->                 
                <div id="promo-image-preview">               
                  <p>Image not selected!</p>
                </div>
              </div>
          </form>
      </div>
    </div>
  </div> 
<?php endif; ?>

<?php if($pagedata->id == 69): ?>
  <div id="Downloads" class="<?= ($pagedata->show_tabs == 0 ? 'singletabdisplay' : 'tabcontent') ?>" style="position: relative;" data-extra="<?=$propertydata->id?>">
      <h1>Manage <?=$propertydata->property_name?> Downloads</h1>
      <div class="downloadslayout" style="/*height: 100%;*/padding-top: 25px;">
        <div class="3imageexp">
          <div class="" style="border: 1px dotted red; padding: 15px; background: rgba(197, 197, 2, 0.23);">
            <p style="float: left;"><a href="#" id="popdownloadmodel">+ Add New Item</a></p>
            <?php if($propertydata->id == 1): ?>
            <div class="downloads-list-filter-controls" style="float: right;">
              <label for="property-selector">Select Property</label>
              <select name="property-selector" id="property-selector">
                <option value="1" <?=($propid == 1 ? 'selected="selected"' : '')?>>All Properties</option>
                <option value="2" <?=($propid == 2 ? 'selected="selected"' : '')?>>Randholee Resorts &amp; Spa</option>
                <option value="3" <?=($propid == 3 ? 'selected="selected"' : '')?>>The Firs</option>
                <option value="4" <?=($propid == 4 ? 'selected="selected"' : '')?>>Ellens Place</option>
              </select>
            </div>
            <?php endif; ?>
            <br style="clear: both;">            
          </div>
        </div>
        <div class="download-item-list-container">
          <div class="download-item-list"></div>
        </div>
      </div>
  </div>

  <!-- Model -->
  <div id="myDownloadModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myDownloadModalLabel">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content" style="padding: 10px;">
          <form id="frmdownloads" method="post" action="<?=SITE_URL.'/ajax-downloads.php'?>" enctype="multipart/form-data">
              <div id="download-details" style="width: 65%;">
                <h3 style="margin-top: 0px;">Download Item</h3>
                <div class="form-group" style="margin-top: 10px;">
                  <label for="downloaditem-property">Property <span class="requiredmsg">(Required)</span></label><br>
                  <select name="downloaditem-property" class="form-control" style="width: 60%;"></select>
                </div>                    
                <div class="form-group" style="margin-top: 10px;">
                  <label for="downloaditem-title">Title <span class="requiredmsg">(Required)</span></label><br>
                  <input type="text" style="" name="downloaditem-title" id="downloaditem-title" value="" class="form-control" />
                </div>
                <div class="form-group" style="">
                  <label for="downloaditem-file">Select File <span class="requiredmsg">(Required)</span></label><br>
                  <input type="file" id="downloaditem-file" name="downloaditem-file"/>
                  <p class="help-block">Example block-level help text here.</p>
                </div>
                <div class="field downloadhiddenstuff">
                  <input type="hidden" name="downloaditem-page-id" value="<?=$pagedata->id?>"/>
                  <input type="hidden" name="downloaditem-action" value=""/>
                  <input type="submit" name="downloaditemsave" id="downloaditemsave" value="Add Item!" class="btn btn-primary"/>                
                </div>
              </div>
          </form>
      </div>
    </div>
  </div> 
<?php endif; ?>
</section>