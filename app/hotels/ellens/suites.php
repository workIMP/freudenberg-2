﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_ellens.php'; ?> 


    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_ellens.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_ellens.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/acc/suites_slider3.jpg') no-repeat 50% 50%; background-size: cover;"></div>    
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/acc/suites_slider2.jpg') no-repeat 50% 50%; background-size: cover;"></div>      
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/acc/suites_slider1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/acc/suites_slider4.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div>

                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="accommodation.php">Accommodation</a></li>
                        <li><span class="arrow"> &gt; </span>Suites</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Suites</div>          
                                <p style="text-align:justify; font-size:18px;">The magnificent suite at 'Ellen's Place' epitomises the grand and graceful style that is evident throughout the property and reflects the essence of living like royalty. Tastefully furnished and lavishly decorated, the suite offers a wealth of modern amenities including a private jacuzzi for guests to unwind in, within the comfort of their room.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:13px; text-transform:none;">The suite at 'Ellen's Place' offers the following in-room amenities and services:</h1>
                                <ul class="accom-list">
                                    <li>Complimentary Mineral Water</li>
                                    <li>Room Service</li>
                                    <li>Daily Housekeeping</li>
                                    <li>In-room Dining</li>
                                    <li>Complimentary Butler Service</li>
                                    <li>Spacious Wardrobe</li>
                                    <li>Air Conditioning with Temperature Control</li>
                                    <li>LCD Televisions</li>
                                    <li>Satellite Channels</li>
                                    <li>DVD Players - On Request</li>
                                    <li>Electric Adapters</li>

                                </ul>
                                <ul class="accom-list">
                                    <li>Writing Table and Chair</li>
                                    <li>Mini-fridge</li>
                                    <li>Bathroom Toiletries</li>
                                    <li>Extra Blankets and Towels</li>
                                    <li>Iron and Board - On Request</li>
                                    <li>Extra Bed - On Request</li>
                                    <li>Baby Crib - On Request</li>
                                    <li>Tea and Coffee-Making Facilities</li>
                                    <li>Wi-Fi Internet Access</li>
                                    <li>Private Jacuzzi</li>
                                </ul>    

                                <div style="clear:both;"></div> 
                                <?php include 'inner_slider.php'; ?>

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->
            <?php include 'trip-advisor.php'; ?>
            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_ellens.php'; ?> 
            </footer>    
    </body>
</html>
