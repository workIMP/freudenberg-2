﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->

        <?php // include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/bat-tennis/bat1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/bat-tennis/bat2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/bat-tennis/bat3.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/bat-tennis/bat4.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div> 
                    
                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  
                
                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="facilities.php">Facilities</a></li>
                        <li><span class="arrow"> &gt; </span>Badminton And Table Tennis</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Badminton And Table Tennis</div>          
                                <p style="text-align:justify; font-size:16px;">Indulging in a game of badminton or table tennis is an excellent way to stay fit during your stay at Randholee. Ideal for family and friends, the table tennis area and the badminton court at the hotel are located in close proximity to each other for convenience and have been designed to offer both a stimulating and relaxing experience. Enjoy complimentary use of badminton racquets, table tennis paddles, shuttlecocks and ping pong balls offered by the hotel. Training can also be provided to beginners on request.</p>  
                                <?php include 'inner_slider.php'; ?> 

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_randolee.php'; ?> 
            </footer>    
    </body>
</html>
