<div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider">
        <?php if (basename($_SERVER['PHP_SELF']) == 'deluxe_m_view.php') { ?>
            <img src="assets/images/rooms/deluxe_m_view/deluxe_m_view1.jpg">
            <img src="assets/images/rooms/deluxe_m_view/deluxe_m_view2.jpg">
            <img src="assets/images/rooms/deluxe_m_view/deluxe_m_view3.jpg">
            <img src="assets/images/rooms/deluxe_m_view/deluxe_m_view4.jpg">
            <img src="assets/images/rooms/deluxe_m_view/deluxe_m_view5.jpg">
            <img src="assets/images/rooms/deluxe_m_view/deluxe_m_view6.jpg">
            <img src="assets/images/rooms/deluxe_m_view/deluxe_m_view7.jpg">
            <img src="assets/images/rooms/deluxe_m_view/deluxe_m_view8.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'deluxe_n_view.php') { ?>
            <img src="assets/images/rooms/deluxe_n_view/deluxe_n_view1.jpg">
            <img src="assets/images/rooms/deluxe_n_view/deluxe_n_view2.jpg">
            <img src="assets/images/rooms/deluxe_n_view/deluxe_n_view3.jpg">
            <img src="assets/images/rooms/deluxe_n_view/deluxe_n_view4.jpg">
            <img src="assets/images/rooms/deluxe_n_view/deluxe_n_view5.jpg">
            <img src="assets/images/rooms/deluxe_n_view/deluxe_n_view6.jpg">
            <img src="assets/images/rooms/deluxe_n_view/deluxe_n_view7.jpg">
            <img src="assets/images/rooms/deluxe_n_view/deluxe_n_view8.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'buffet.php') { ?>
            <img src="assets/images/cuisine/buffet/buf1.jpg">
            <img src="assets/images/cuisine/buffet/buf2.jpg">
            <img src="assets/images/cuisine/buffet/buf3.jpg">
            <img src="assets/images/cuisine/buffet/buf4.jpg">
            <img src="assets/images/cuisine/buffet/buf5.jpg">
            <img src="assets/images/cuisine/buffet/buf6.jpg">
            <img src="assets/images/cuisine/buffet/buf7.jpg">
            <img src="assets/images/cuisine/buffet/buf8.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'bar.php') { ?>
            <img src="assets/images/cuisine/bar/bar1.jpg">
            <!--<img src="assets/images/cuisine/bar/bar2.jpg">
            <img src="assets/images/cuisine/bar/bar3.jpg">-->
            <img src="assets/images/cuisine/bar/bar4.jpg">
            <img src="assets/images/cuisine/bar/bar5.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'signature_dining.php') { ?>
            <img src="assets/images/cuisine/sig_dining/sign1.jpg">
            <img src="assets/images/cuisine/sig_dining/sign2.jpg">
            <img src="assets/images/cuisine/sig_dining/sign3.jpg">
            <img src="assets/images/cuisine/sig_dining/sign4.jpg">
            <img src="assets/images/cuisine/sig_dining/sign5.jpg">
            <img src="assets/images/cuisine/sig_dining/sign6.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'squash-court.php') { ?>
            <img src="assets/images/squash_court/sc1.jpg">
            <img src="assets/images/squash_court/sc2.jpg">
            <img src="assets/images/squash_court/sc3.jpg">
            <img src="assets/images/squash_court/sc4.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'badminton-and-table-tennis.php') { ?>
            <img src="assets/images/badminton_and_table_tennis/bm1.jpg">
            <img src="assets/images/badminton_and_table_tennis/bm2.jpg">
            <img src="assets/images/badminton_and_table_tennis/bm3.jpg">
            <img src="assets/images/badminton_and_table_tennis/bm4.jpg">
            <img src="assets/images/badminton_and_table_tennis/bm5.jpg">
            <img src="assets/images/badminton_and_table_tennis/bm6.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'shopping-boutique.php') { ?>
            <img src="assets/images/shopping_boutique/sb1.jpg">
            <!--<img src="assets/images/shopping_boutique/sb2.jpg">
            <img src="assets/images/shopping_boutique/sb3.jpg">
            <img src="assets/images/shopping_boutique/sb4.jpg">-->
            <img src="assets/images/shopping_boutique/sb5.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'spa.php') { ?>
            <img src="assets/images/spa/spa1.jpg">
           <!-- <img src="assets/images/spa/spa2.jpg">-->
            <img src="assets/images/spa/spa5.jpg">
            <img src="assets/images/spa/spa3.jpg">
            <img src="assets/images/spa/spa4.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'infinity-pool.php') { ?>
            <img src="assets/images/infinity_pool/ip1.jpg">
            <img src="assets/images/infinity_pool/ip2.jpg">
            <img src="assets/images/infinity_pool/ip3.jpg">
            <img src="assets/images/infinity_pool/ip4.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'wedding.php') { ?>
            <img src="assets/images/wedding/wed1.jpg">
            <img src="assets/images/wedding/wed2.jpg">
            <img src="assets/images/wedding/wed3.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'conferences-and-workshops.php') { ?>
            <img src="assets/images/conferences_and_workshops/conf2.jpg">
            <img src="assets/images/conferences_and_workshops/conf3.jpg">
            <img src="assets/images/conferences_and_workshops/conf4.jpg">
            <img src="assets/images/conferences_and_workshops/conf5.jpg">
            <img src="assets/images/conferences_and_workshops/conf6.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'fitness-centre.php') { ?>
            <img src="assets/images/fitness_centre/fc1.jpg">
            <img src="assets/images/fitness_centre/fc2.jpg">
            <img src="assets/images/fitness_centre/fc3.jpg">
            <img src="assets/images/fitness_centre/fc4.jpg">
            <img src="assets/images/fitness_centre/fc8.jpg">
            <img src="assets/images/fitness_centre/fc5.jpg">
            <img src="assets/images/fitness_centre/fc6.jpg">
            <img src="assets/images/fitness_centre/fc7.jpg">
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'business-centre.php') { ?>
            <img src="assets/images/business_centre/bc1.jpg">
            <img src="assets/images/business_centre/bc2.jpg">
            <img src="assets/images/business_centre/bc3.jpg">
        <?php } ?>
    </div>
</div>