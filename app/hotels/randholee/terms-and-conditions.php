<?php 
$pg = ['property' => 'randholee', 'page' => 'terms-and-conditions'];
?>
<!DOCTYPE html>
<html class="no-js">
<?php include '../../includes/header_randholee.php';?> 

<body class="node-type-accommodation-list">
<header id="header" role="banner">
  <?php include '../../includes/navigation_randholee.php';?> 
  </header><!--  #header  -->
  
<?php include '../../includes/booking_randholee.php'; ?> 
<div class="blur">  
  
  <div id="node-6" class="node--accommodation_list mode--full">

  <main id="main" role="main">
    <article role="article">
      <div class="ctatext-wrapper">
      
      <!--<div id="route" style="margin-top: 10px;">
            <breadcrumb class="menu">
                <li><a href="index.php">Home</a></li>
                <li><span class="arrow"> &gt; </span>Terms and Conditions</li>
            </breadcrumb>
        </div>-->
      
        <div class="ctatext-text">         
              <?php require '../../includes/showdescription.php'; ?>            
        </div><!--  .ctatext-text  -->
      </div><!--  .ctatext-wrapper  -->                          
    </article>      
          
  </main>   
</div><!--  #node-details  -->
  
    <div style="clear:both"></div>
  <footer id="footer" role="contentinfo">  
  <?php include '../../includes/footer_randolee.php';?> 
   </footer>    
</body>
</html>
