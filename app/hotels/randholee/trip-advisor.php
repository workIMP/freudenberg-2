<style>.ctatext-wrapper{padding-top: 20px;padding-bottom: 20px;}</style>
<div id="nt-example1-container">
    <aside role="complementary">
        <div class="ctatext-buildadventure ctatext-wrapper" style="background:#f2f2f2;">
            <div class="col-md-12 ctatext-text">
                <div class="hdr-two" style="font-style: italic; color:#484848;">Guest Comments</div>
                <ul id="nt-example1">
                    
                    <li class="news-item" >
                        “The views of this hotel are unbelievable...”
                        <br>
                        <div style="font-weight:700; font-style:italic;text-indent:450px">-MattS1987, Malta</div>
                    </li>

                    <li class="news-item" >
                        “Breathtaking views of the hills with the infinity pool...”
                        <br>
                        <div style="font-weight:700; font-style:italic;text-indent:450px">-kbanerjee2015, India</div>
                    </li>

                    <li class="news-item" >
                        “What a beautiful hotel! Warm welcome and great rooms...”
                        <br>
                        <div style="font-weight:700; font-style:italic;text-indent:450px">-marlene g, South Africa</div>
                    </li>
                    
                </ul>
            </div>
        </div>
    </aside>
</div>
