﻿<!DOCTYPE html>

<html class="no-js">

    <?php include '../../includes/header_firs.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 

        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">  

            <div class="node--page_basic mode--full">  
                <aside role="complementary">
                    <?php include '../../includes/_slider_firs.php'; ?>
                </aside>   

                <div id="route" style="margin-left: 21.3% !important;">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Promotions</li>
                    </breadcrumb>
                </div> 

                <aside role="complementary">
                    <div class="grid">
                        <div class="hdr-two" style="text-align: left; padding: 10px; font-size:1.2em;">THE FIRS PROMOTIONS</div>
                        <div style="clear:both"></div>
                        <figure class="effect-chico">
                            <img src="assets/images/early_promo.jpg" alt="img10"/>
                            <figcaption>
                                <h2>SAVE 20% WITH ADVANCE <span>PURCHASE DISCOUNT</span></h2>
                                <p>Time to think ahead! Make your booking 30 days in advance and enjoy 20% off on any room or meal plan</p>
                                <a href="advance-purchase.php">View more</a>
                            </figcaption>			
                        </figure>

                        <figure class="effect-chico">
                            <img src="assets/images/longstay_promo.jpg" alt="img10"/>
                            <figcaption>
                                <h2>LONG STAY OFFER ON OCTOBER TO FEBRUARY - <span>SUITE ROOMS</span></h2>
                                <p>GET THE 4th NIGHT FREE Suite rooms on Room Only / Bed & Breakfast plans</p>
                                <a href="#">View more</a>
                            </figcaption>			
                        </figure>

                        <figure class="effect-chico">
                            <img src="assets/images/lastmin_promo.jpg" alt="img10"/>
                            <figcaption>
                                <h2>40% LAST MINUTE DISCOUNT - <span>SUITE ROOMS</span></h2>
                                <p>GET THE 4th NIGHT FREE Suite rooms on Room Only / Bed & Breakfast plans</p>
                                <a href="#">View more</a>
                            </figcaption>			
                        </figure>
                    </div><!--  .ctacard-wrapper  -->
                </aside>    

            </div>  

            <footer id="footer" role="contentinfo"> 

                <?php include 'trip-advisor.php'; ?>
                <?php /* ?>    <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>

                <?php include '../../includes/footer_firs.php'; ?>



                </body>

                </html>