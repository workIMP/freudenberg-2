﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_firs.php'; ?>

    <body class="html page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <h1 class="hide-visual">The Firs - About Us</h1>

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_1.jpg') no-repeat 50% 50%; background-size: cover;"></div>    
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>About Us</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">About Us</div>   
                                <p style="text-align:justify; font-size:16px;">‘The Firs’ is an authentic Sri Lankan up-market Heritage Bungalow which was originally built over a century ago by Philip Freudenberg - Ceylon’s first International Counsel General for Germany. It later became the holiday home of Hon. D. S. Senanayake, Sri Lanka’s first Prime Minister, who visited it quite often in pursuance of golf, bridge-playing and photography. Ownership of the bungalow then passed on to his two sons Dudley and Robert Senanayake and the bungalow has remained with the family for over 100 years to date.</p>

                                <p style="text-align:justify; font-size:16px;">Beckoning historical value, The Firs has been conceptualized according to the British Colonial era with the intention of providing the same experience during the British Monarchy. After continuous upgrades, it now follows the very same concept, ensuring that every aspect of the property exudes comfort to reflect the unique spirit and soul of its location, providing guests with a one-of-a-kind experience.</p>



        <!--<img src="assets/images/abt-us.jpg" width="80%">
        
        <video width="320" height="240" autoplay loop>
        <source src="assets/video/Sequence1_1.mp4" type="video/mp4">
     </video>-->

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>

            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_firs.php'; ?> 
            </footer>    
    </body>
</html>
