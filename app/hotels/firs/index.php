﻿<?php 
$pg = ['property' => 'firs', 'page' => 'home'];
include '../../includes/header_firs.php'; 
?>
    <body class="node-type-accommodation-list">

        <header id="header" role="banner">
            <h1 class="hide-visual">The Firs - The Firs Home Page</h1>  

            <?php include '../../includes/navigation_firs.php'; ?> 

        </header><!--  #header  -->
        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">  
            <div class="node--page_basic mode--full">    
                <aside role="complementary">
                    <header>
                        <h1 class="hide-visual">The Firs Slideshow</h1>
                    </header>    
                    <?php include '../../includes/slider_firs.php'; ?>
                </aside>  

                <aside role="complementary">
                    <header>
                        <h1 class="hide-visual">Randholee Luxury Resort Experiences</h1>
                    </header> 

                    <div id="main" role="main">
                        <div class="wrapper">
                            <article role="article">
                                <div class="ctatext-wrapper">
                                    <div class="ctatext-text">
                                        <?php require '../../includes/showdescription.php'; ?>
                                    </div><!--  .ctatext-text  -->          
                                </div><!--  .ctatext-wrapper  -->    
                            </article>
                        </div><!--  .wrapper  -->
                    </div><!--  #main  -->


                    <div class="experience-thumblist highlight-panels">
                        <ul>  
                            <div class="experience-thumblist highlight-panels">
                                <?php require '../../includes/shownavigation-5.php'; ?>
                            </div><!--  .experience-thumblist .highlight-panels  --> 
                        </ul>
                    </div><!--  .experience-thumblist .highlight-panels  -->
                </aside>

            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <?php include 'trip-advisor.php'; ?> 

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">    

                <?php /* ?>    <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>


                <?php include '../../includes/footer_firs.php'; ?>

                </body>
                </html>