<?php 
    $hostname = str_replace("www.", "", $_SERVER['HTTP_HOST']);

    $token = NoCSRF::generate('token');
?>

<nav id="nav-primary-wrapper" class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
	
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
	<div class="logo"><a href="http://freudenbergleisure.com/" class="logo_bg">Freudenberg Leisure - Home</a></div>  
    </div>
	<div class="hidden-lg hidden-md" style="position:absolute; margin-top: -19%; margin-left: 59%; width: 16%; border:0px;">
			<a href="http://www.bestweb.lk/vote/voteme.php?domain=freudenbergleisure.lk"><img src="assets/img/vote4us.png" alt="logo" width="150" height="150" style="margin-top:20px" /></a>
		</div>
    <div class="collapse navbar-collapse" id="myNavbar"> 	
      <ul class="nav navbar-nav test  hidden-xs">
        <li class="active"><a href="http://freudenbergleisure.com/">Home</a></li>
		<li class="dropdown">
			<a href="#" class="dropbtn">Hotels</a>
			<div class="dropdown-content ">
				<a title="Randholee Luxury Resorts" href="http://randholeeresort.<?php echo $hostname; ?>/" ><div id="randh"></div><span class="hide">Randholee Luxury Resorts</span></a>
				<a title="The Firs" href="http://firs.<?php echo $hostname; ?>/" ><div id="firs"></div><span class="hide">The Firs</span></a>
				<a title="Ellen's Place" href="http://ellensplace.<?php echo $hostname; ?>/"><div id="ellens"></div><span class="hide">Ellen's Place</span></a>
			</div>
		</li>
        <li><a href="experience">Experiences</a></li>
        <li><a href="promotions">Special Offers</a></li>
		<li><a href="accommodation">Accommodation</a></li>
		<li><a href="about-us">About Us</a></li>
		<li><a href="contact-us">Contact Us</a></li> 
      </ul>
       <ul class="nav navbar-nav test1 hidden-xs">
        <li><a href="#" style="font-style: italic; font-size: 14px; margin-top: 8%;"> Tel: +94 11 2445282</a></li>
        <li>
			<div class="search">
                <form method="POST" action="search-results.php">
                    <input name="search" type="text" size="15" class="input" required>
                    <input type="hidden" name="token" value="<?php echo $token ?>">
                    <input type="submit" class="submit" value="SEARCH" style="background:none; border: none;">
                    <div class="clear"></div>
                </form>
            </div>
		</li>
      </ul> 
	
		<div class="panel-group visible-xs" id="accordion" role="tablist" aria-multiselectable="true" style="left:0px; margin-top: 3%;">

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a  href="" >
							<i class="more-less glyphicon glyphicon-plus"></i>
							Home
						</a>
					</h4>
				</div> 
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingTwo">
					<h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							<i class="more-less glyphicon glyphicon-plus"></i>
							Hotels
						</a>
					</h4>
				</div>
				<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
					<div class="panel-body" style="color: #0077C1;">
							<ul>
								<li><a href="http://randholeeresort.<?php echo $hostname; ?>/">Randholee Luxury Resorts </a></li>
								<li><a href="http://firs.<?php echo $hostname; ?>/">The Firs</a></li>
								<li><a href="http://ellensplace.<?php echo $hostname; ?>/">Ellen's Place</a></li>
							</ul>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingThree">
					<h4 class="panel-title">
						<a href="experience" >
							<i class="more-less glyphicon glyphicon-plus"></i>
							Experiences
						</a>
					</h4>
				</div> 
			</div>
			
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingFour">
					<h4 class="panel-title">
						<a  href="promotions" >
							<i class="more-less glyphicon glyphicon-plus"></i>
							Special Offers
						</a>
					</h4>
				</div> 
			</div>
			
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingFive">
					<h4 class="panel-title">
						<a  href="accommodation" >
							<i class="more-less glyphicon glyphicon-plus"></i>
							Accommodation
						</a>
					</h4>
				</div> 
			</div>
			
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingSix">
					<h4 class="panel-title">
						<a href="about-us" >
							<i class="more-less glyphicon glyphicon-plus"></i>
							About Us
						</a>
					</h4>
				</div> 
			</div>
			
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingSeven">
					<h4 class="panel-title">
						<a href="contact-us">
							<i class="more-less glyphicon glyphicon-plus"></i>
							Contact Us
						</a>
					</h4>
				</div> 
			</div>

		</div>  
 
    </div>
	
	<div class="prime-quick-link quick-link-contact lanuge ddd hidden-xs">
		 
        <div id="google_translate_element"></div><script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL, autoDisplay: false}, 'google_translate_element');
                }
        </script>
        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    </div>
  </div>
</nav>


 