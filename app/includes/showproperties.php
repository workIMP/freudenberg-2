<?php $properties = $cms->getProperties(); ?>
<?php foreach($properties as $prop): ?>
    <div class="ctacard-card equal-height">
        <div class="inner-ratio">
            <!--<img class="img-responsive" src="assets/images/ellens/sl1.jpg" alt="Ellen's Place">-->
            <img src="assets/img/img-1.png" class="lazy" data-src="images/properties/<?=$prop->prop_image?>" alt="<?=$prop->prop_name?>">
        </div><!--  .inner-ratio  -->    
        <div class="ctacard-text">    
            <h2 class="hdr-four"><?=$prop->prop_name?></h2>    
            <p><?=$prop->prop_description?></p>    
        </div><!--  .ctacard-text  -->    
        <div class="ctacard-button">
            <a href="<?=$prop->prop_link?>" class="btn-arrow">Find Out More</a>  
        </div><!--  .ctacard-button  -->      
    </div><!--  .ctacard-card  -->	
<?php endforeach; ?>