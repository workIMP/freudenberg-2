<?php if(BESTWEB): ?>
	
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<?php

        switch($pg['page'])
        {
            default:
            case 'home':
            ?>
                <title>Small Luxury Hotels, Sri Lanka - Freudenberg Leisure - Leisure Hotels Sri Lanka</title>
                <meta name="description" content="Freudenberg Leisure Sri Lanka, the ultimate in small luxury hotels." />
                <meta name="keywords" content=" Freudenbergleisure Sri Lanka, Randholee resorts Sri lanka, Ellens place Sri Lanka, The firs Sri Lanka, luxury hotels in Sri Lanka, luxury boutique hotels in Sri Lanka" />
            <?php
            break;

            case 'experience':
            ?>
                <title>Freudenberg Leisure - Experiences, Tourist Destinations</title>
                <meta name="description" content="Across our three properties we offer a range of activities and excursions you can experience while enjoying our hospitality and world class service." />
                <meta name="keywords" content="Freudenbergleisure Sri Lanka, Randholee resorts Sri lanka, Ellens place Sri Lanka, The firs Sri Lanka, luxury hotels in Sri Lanka, luxury boutique hotels in Sri Lanka, Excursions in Sri Lanka, Sri Lanka tourist trips"  />            
            <?php
            break;

            case 'promotions':
            ?>
                <title>Freudenberg Leisure - Promotions and Special Offers</title>
                <meta name="description" content="Get the best deals for rooms and dining at our luxury hotels." />
                <meta name="keywords" content="Randholee offers, Ellens place offers, Firs special offers, Sri Lanka hotel offers, Freudenberg leisure offers, Freudenbergleisure Sri Lanka, Randholee resorts Sri lanka, Ellens place Sri Lanka, The firs Sri Lanka, luxury hotels in Sri Lanka, luxury boutique hotels in Sri Lanka, Excursions in Sri Lanka, Sri Lanka tourist trips"  />            
            <?php
            break;

            case 'accommodation':
            ?>
                <title>Hotels in Sri Lanka - Freudenberg Leisure - Accommodation</title>
                <meta name="description" content="We offer luxurious accommodation options in Colombo, Kandy and Nuwara Eliya. We offer round-trip packages for tourists who would like to stay in all 3 cities." />
                <meta name="keywords" content="Colombo accommodation, Kandy accommodation, Nuwara Eliya accommodation, Freudenbergleisure Sri Lanka, Randholee resorts Sri lanka, Ellens place Sri Lanka, The firs Sri Lanka, luxury hotels in Sri Lanka, luxury boutique hotels in Sri Lanka, Excursions in Sri Lanka, Sri Lanka tourist trips"  />           
            <?php
            break;

            case 'aboutus':
            ?>
                <title>Hotels in Colombo - Freudenberg Leisure - About Us</title>
                <meta name="description" content="Have a vacation like never before in the coastal paradise of Sri Lanka. Our hotels are located around the top tourist destinations in the country." />
                <meta name="keywords" content="About Freudenberg leisure, Freudenbergleisure Sri Lanka, Randholee resorts Sri lanka, Ellens place Sri Lanka, The firs Sri Lanka, luxury hotels in Sri Lanka, luxury boutique hotels in Sri Lanka, Excursions in Sri Lanka, Sri Lanka tourist trips"  />
            <?php
            break;

            case 'contactus':
            ?>
                <title>Freudenberg Leisure - Contact Us</title>
                <meta name="description" content="Get in touch with the Freudenberg Leisure team for more information." />
                <meta name="keywords" content="Freudenberg leisure contact details, contact Freudenberg leisure, Freudenbergleisure Sri Lanka, Randholee resorts Sri lanka, Ellens place Sri Lanka, The firs Sri Lanka, luxury hotels in Sri Lanka, luxury boutique hotels in Sri Lanka, Excursions in Sri Lanka, Sri Lanka tourist trips"  />
            <?php
            break;

            case 'downloads':
            ?>
                <title>Freudenberg Leisure - Downloads</title>
                <meta name="description" content="Freudenberg Leisure downloads. Factsheets and brochures." />
                <meta name="keywords" content="Randholee factsheet, Ellens place factsheet, Firs factsheet, Freudenberg Leisure brochure, Freudenberg leisure, Freudenbergleisure Sri Lanka, Randholee resorts Sri lanka, Ellens place Sri Lanka, The firs Sri Lanka, luxury hotels in Sri Lanka, luxury boutique hotels in Sri Lanka, Excursions in Sri Lanka, Sri Lanka tourist trips"  />
            <?php
            break;

            case 'tandc':
            ?>
                <title>Freudenberg Leisure - Terms and Conditions</title>
                <meta name="description" content="These are our Terms & Conditions, Company Policies"/> 
                <meta name="keywords" content="Freudenberg leisure, Freudenbergleisure Sri Lanka, Randholee resorts Sri lanka, Ellens place Sri Lanka, The firs Sri Lanka, luxury hotels in Sri Lanka, luxury boutique hotels in Sri Lanka, Excursions in Sri Lanka, Sri Lanka tourist trips"  />
            <?php
            break;

            case 'pp':
            ?>
                <title>Freudenberg Leisure - Privacy Policy</title>
                <meta name="description" content="AV15 is a high quality apartment hotel in Colombo at an affordable price." />
                <meta name="keywords" content="Colombo Hotels, Colombo Accommodation, Colombo Rooms, Budget Hotel Colombo, Cheap Hotels Colombo, Sri Lanka Hotels, Apartment Hotels Colombo, AirBNB Hotel Colombo"  />
            <?php
            break;

            case 'sitemap':
            ?>
                <title>Freudenberg Leisure - Site Map</title>
                <meta name="description" content="Navigate easily to the desired page of the Freudenberg Leisure, ultimate in luxury hotels." />
                <meta name="keywords" content="Colombo accommodation, Kandy accommodation, Nuwara Eliya accommodation, Freudenbergleisure Sri Lanka, Randholee resorts Sri lanka, Ellens place Sri Lanka, The firs Sri Lanka, luxury hotels in Sri Lanka, luxury boutique hotels in Sri Lanka, Excursions in Sri Lanka, Sri Lanka tourist trips"  />
            <?php
            break;

            case 'error':
            ?>
                <title>Freudenberg Leisure - Official Site</title>
                <meta name="description" content="Welcome to the official website of Freudenberg Leisure." />
                <meta name="keywords" content="Colombo Hotels, Colombo Accommodation, Colombo Rooms, Budget Hotel Colombo, Cheap Hotels Colombo, Sri Lanka Hotels, Apartment Hotels Colombo, AirBNB Hotel Colombo"  />
            <?php
            break;

        }

    ?>

<?php else: ?>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MM4HKC"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start':
                    new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
                '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-MM4HKC');</script>
<!-- End Google Tag Manager -->

<?php endif; ?>