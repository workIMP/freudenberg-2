<?php
require '../appdata/cms/bootstrap.php';

	//$navdata = $db->getRow('SELECT * FROM tblnavigation WHERE id = ?', array($descpid));
	$pageslist = $db->getRows('SELECT id, page_name, child_of FROM tblpages WHERE property_id = ? ORDER BY id ASC', array((int)$_GET['prid']));


	//$pages = [];
	$subpages = array();
	$mainpages = array();

	foreach($pageslist as $page)
	{
		//$pages[] = $page->child_of;
		if($page->child_of > 0)
		{
			$subpages[$page->child_of][] = $page;
			//echo $page->child_of;
		}
		else
		{
			$mainpages[] = $page;
		}
	}

	echo json_encode(array(
			'main_pages' => $mainpages,
			'sub_pages' => $subpages
		));