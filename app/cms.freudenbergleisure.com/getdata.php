<?php
require '../appdata/cms/bootstrap.php';

	// get navigation item list
	$descpid = (int)$_GET['pgid'];

	//$navdata = $db->getRow('SELECT * FROM tblnavigation WHERE id = ?', array($descpid));
	$descdata = $db->getRow('SELECT * FROM tbldescriptions WHERE `page_id` = ?', array($descpid));

	if(!$descdata)
	{
		$dummydata = new StdClass;
		$dummydata->sub_title = '';
		$dummydata->main_title = '';
		$dummydata->body_text = '';
		$dummydata->id = 'd';

		echo json_encode($dummydata);
	}
	else
	{
		echo json_encode($descdata);		
	}