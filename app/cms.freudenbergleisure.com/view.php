<?php
require_once '../appdata/cms/bootstrap.php';

	$property_slug = $_GET['property'];

	$propertydata = $db->getRow('SELECT * FROM tblproperties WHERE `property_slug` = ?', array($property_slug));

	//echo $property->id;

	$permission = hasPermission($_SESSION['USER'], $propertydata->id);

	/** debugging
	if($permission)
	{
		echo 'has p';
	}
	else
	{
		echo 'no p';
	}*/

	//echo $permission;
	//exit;

	if(!$permission)
	{
		header('Location: '.SITE_URL.'/');
		exit;
	}


	// set title
	$pagetitle = 'Manage '.$propertydata->property_name.' Contents';

	// get pages
	$pages = $db->getRows('SELECT * FROM tblpages WHERE `child_of` = ? AND `property_id` = ? AND `visible_in` = ?', array(0, $_SESSION['USER']->property_id, 1));

	$subpages = $db->getRows('SELECT * FROM tblpages WHERE `child_of` != ? AND `property_id` = ? AND `visible_in` = ?', array(0, $_SESSION['USER']->property_id, 1));

	$promopages = $db->getRows('SELECT * FROM tblpages WHERE `child_of` = ? AND `property_id` = ? AND `visible_in` = ?', array(29,$_SESSION['USER']->property_id, 3));

	$submenuitems = [];

	foreach($pages as $pg)
	{
		// check if this $pg has sub pages
		foreach($subpages as $subpg)
		{
			if($subpg->child_of == $pg->id)
			{
				// sub page. add $subpg->id to the subpages array
				$submenuitems[$pg->id][] = $subpg;
			}
		}
	}

	// set template
	$template = $propertydata->property_tpl;


	// if there is a form submit, that is for updating property details. 
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$errors = false;
		$errfields = [];

		$filetypes = [
				'image/jpeg' => '.jpg',
				'image/png'  => '.png'
		];

		if(!isset($_POST['address']) || empty($_POST['address']))
		{
			$errors = true;
			$errfields['address'] = '';
		}

		if(!isset($_POST['telephone']) || empty($_POST['telephone']))
		{
			$errors = true;
			$errfields['telephone'] = '';
		}

		if(!isset($_POST['fax']) || empty($_POST['fax']))
		{
			$errors = true;
			$errfields['fax'] = '';
		}

		if(!isset($_POST['email']) || empty($_POST['fax']))
		{
			$errors = true;
			$errfields['email'] = '';
		}

		if(isset($_FILES['logofile']['name']) && !empty($_FILES['logofile']['name']))
		{
			if(!array_key_exists($_FILES['logofile']['type'], $filetypes))
			{
					$errors = true;
					$errfields['logofile'] = 'Invalid File Type!';
			}
			else
			{
				if($_FILES['logofile']['size'] > 1000000)
				{
					$errors = true;
					$errfields['logofile'] = 'Image should be below 1MB in Size.';
				}
			}
		}


		if(!isset($_POST['longitude']) || empty($_POST['longitude']))
		{
			$errors = true;
			$errfields['longitude'] = '';
		}

		if(!isset($_POST['latitude']) || empty($_POST['latitude']))
		{
			$errors = true;
			$errfields['latitude'] = '';
		}

		if(!$errors)
		{
			// good -> upload image

			if(isset($_FILES['logofile']['name']))
			{
				if(is_uploaded_file($_FILES['logofile']['tmp_name']))
				{
					$ext = $filetypes[$_FILES['logofile']['type']];
					$newfilename = time().md5(mt_rand()).strtolower($property_slug).$ext;
					$updpath = APP_ROOT .'/public_html/images/logos/';

					move_uploaded_file($_FILES['logofile']['tmp_name'], $updpath.$newfilename);

				}

				//$updateqry = 'UPDATE tblproperties SET 	`property_address`=?, `property_telephone`=?,`property_fax`=?,`property_email`=?,`property_logo`=?,`property_map_longitude`=?,`property_map_latitude`=? WHERE `id` = ?';				
			}

			// update database details
			$updata = array($_POST['address'],$_POST['telephone'],$_POST['fax'],$_POST['email'],$_POST['longitude'],$_POST['latitude'],$_POST['propertyid']);
			$updres1 = $db->updateRecord('UPDATE tblproperties SET `property_address`=?, `property_telephone`=?, `property_fax`=?, `property_email`=?, `property_map_longitude`=?, `property_map_latitude`=? WHERE `id`=?', $updata);

			// update logo image
			if(isset($newfilename))
			{
				$updres2 = $db->updateRecord('UPDATE tblproperties SET `property_logo`=? WHERE `id`=?', array($newfilename, $_POST['propertyid']));
			}


			// redirect
			header('Location: '.SITE_URL.'/manage/'.strtolower($property_slug).'?detailsupdated');
			
		}
	}



	// handle actions
	switch($_GET['action'])
	{
		case 'addpage':

			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				// posted -> validate
				$errors = false;
				$errmsg = [];

				if(!isset($_POST['pagename']) || empty($_POST['pagename']))
				{
					$errors = true;
					$errmsg[] = 'Page name is required!';
				}

				if(!isset($_POST['pagetemplate']) || empty($_POST['pagetemplate']))
				{
					$errors = true;
					$errmsg[] = 'Plese select the page template';
				}

				if(!isset($_POST['pageslug']) || empty($_POST['pageslug']))
				{
					$errors = true;
					$errmsg[] = 'Please enter the page slug. (eg: page-slug)';
				}


				// set page options
				$desc = (in_array('desc', $_POST['pgoptions']) ? 1 : 0);
				$nav = (in_array('nav', $_POST['pgoptions']) ? 1 : 0);
				$slider = (in_array('slider', $_POST['pgoptions']) ? 1 : 0);

				if(in_array('slider', $_POST['pgoptions']))
				{
					// slider ok
					// what type of it? Default or Custom?

					$slider = (int)$_POST['slideropt'];
				}
				else
				{
					$slider = 0;
				}

				$gallery = (in_array('gallery', $_POST['pgoptions']) ? 1 : 0);
				$allowsub = (in_array('sub', $_POST['pgoptions']) ? 1 : 0);


				if(!$errors)
				{

					$pgaddqry = $db->addRecord('INSERT INTO tblpages (`page_name`,`page_template`,`child_of`,`page_slug`,`property_id`,`section_desc`,`section_nav`,`section_slider`,`section_gallery`,`allow_subpages`) VALUES(?,?,?,?,?,?,?,?,?,?)', array($_POST['pagename'], $_POST['pagetemplate'], (isset($_POST['parentpg']) ? (int)$_POST['parentpg'] : 0), $_POST['pageslug'], $_SESSION['USER']->property_id, $desc, $nav, $slider, $gallery, $allowsub));
					
					if(!$pgaddqry)
					{
						throw new Exception('Error adding a new page!');
					}

					// success

					// if parent is 'promotions' add a record to tblpromotionpages
					if(in_array($_POST['parentpg'], array(70,71,72,73)))
					{
						$db->addRecord('INSERT INTO tblpromotionpages (`promotion_id`,`page_id`) VALUES(?,?)', array((int)base64_decode($_GET['extra']), $pgaddqry));
						$db->updateRecord('UPDATE tblpages SET `visible_in` = ? WHERE `id` = ?', array(1,$pgaddqry));
					}


					//$_SESSION['_msg'] = $_POST['pagename'];

					//header('Location: '.SITE_URL.'/manage/'.$_GET['property'].'/?pageadded');

					$success = $_POST['pagename'];

					unset($_POST);

					//header('Location: '.SITE_URL.'/manage/'.$_GET['property'].'/addpage?pg='.urlencode($success));
					header('Refresh:0; url='.SITE_URL.'/manage/'.$_GET['property'].'/edit/'.base64_encode($pgaddqry));
				
				}
			}

			$pageTitle = 'Add new page - '.$propertydata->property_name;

			// scan page templates and add to a list
			$scannedtplfiles = scandir(APP_ROOT.'/templates/pages/');
			//print_r($scannedtplfiles);
			foreach($scannedtplfiles as $tplfile)
			{
				if(!in_array($tplfile, array('.','..')))
				{
					if(!is_dir($tplfile))
					{
						$tplfiles[] = $tplfile;
					}
				}
			}

			//print_r($tplfiles);

			if(isset($_GET['id']))
			{
				$parentid = base64_decode(urldecode($_GET['id']));

				$pagedata = $db->getRow('SELECT * FROM tblpages WHERE id = ?', array($parentid));

				$pageTitle = $pagedata->page_name . ' -> Add Sub Page';
			}

			$template = 'add-new-page.tpl';			

		break;

		case 'edit':
			$pageid = base64_decode(urldecode($_GET['id']));
			//echo $pageid;
			$pagedata = $db->getRow('SELECT * FROM tblpages WHERE `id` = ?', array($pageid));

			if($pagedata->child_of > 0)
			{
				// get parent page name
				$parentpg = $db->getRow('SELECT * FROM tblpages WHERE `id` = ?', array($pagedata->child_of));
			}

			//$descdata = $db->getRow('SELECT * FROM tbldescriptions WHERE `page_id` = ?', array($pagedata->id));

			// get page data from tables (based on its contents, desc, nav etc)

			$template = 'pages/'.$pagedata->page_template;

		break;

		case 'promotions':
		break;

		case 'menu':
			$pageTitle = 'Manage Menu - '.$propertydata->property_name;
			$template = 'manage-menu.tpl';
		break;

		default:
		break;
	}


	require '../appdata/cms/templates/main.tpl';
