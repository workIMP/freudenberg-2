<?php
header('Content-Type: application/javascript');
?>

var pg = '<?php echo $_GET['pg']; ?>';

$.get('http://freudenberg.cms/getnavitemlist.php', { pgid: pg }, function(navitemlist){
	$('.nav-item-list').html(navitemlist);
	//console.log(navitemlist);
})

$('#popme').on('click', function(e){
	e.preventDefault();
	console.log('clicked');
	$('#frmnavigation input[name=navitem-action]').val("add");
	//$('#frmnavigation').append('<input type="hidden" name="navtype" value="1"/>');
	//$('#frmnavigation input[name=navitem-layout]').val("1");  -> not using
	$('#myModal').modal('show');
});

/* not using anymore
$('#popme2').on('click', function(e){
	e.preventDefault();
	console.log('clicked');
	$('#frmnavigation input[name=navitem-layout]').val("2");
	$('#myModal').modal('show');
});*/

// remove (Required) on focus
$('#frmnavigation :input').on('focus', function(){
	//console.log('focused');
	$(this).siblings('label').children('.requiredmsg').css('display', 'none');
})

$('#frmnavigation').on('submit', function(e){
	e.preventDefault();

	var navform = $(this);

	var errors = false;
	var formData = new FormData();

	var needfile = true;

	//console.log(formData);

	//formData.append('hello', 'ami');		-> test append (works)

	//console.log(formData.getAll('hello')); 	-> debug (works)

	// validate each input element
	$.each($(this).find(':input'), function(){
		//console.log($(this).attr('name'));

		element = $(this);


		if(element.val() == '')
		{
			// skip checking image file for upadtes
			if(element.attr('name') == 'navitem-image')
			{
				if(navform.find('input[name=navitem-action]').val() == 'update')
				{
					// skip iteration
					return true;
				}
			}

			errors = true;
			element.siblings('label').children('.requiredmsg').css('display','inline');
		}
		else
		{
			// append to form data. exclude navitem-image
			if(element.attr('name') != 'navitem-image')
			{
				//payload[element.attr('name')] = element.val();
				formData.append(element.attr('name'), element.val());
			}
			
		}
	})

	/* this also does what above does.
	$(this).find(':input').each(function(index, element){

		console.log(element.val());
	})*/


	/* this is not good coding. so disabled it
	var navitemname = $(this).find('input[name=navitem-name]');
	var navitemtargetpg = $(this).find('select[name=navitem-target]');
	var navitemtagline = $(this).find('input[name=navitem-tagline]');
	var navitemdesc = $(this).find('textarea[name=navitem-desc]');
	var navitemimage = $(this).find('input[name=navitem-image]');
	var navitemlayouttype = $(this).find('input[name=navitem-layout]');
	var navitempageid = $(this).find('input[name=navitem-page-id]');

	//console.log(navitemtargetpg.val());

	// validate items
	var itemstovalidate = [navitemname,navitemtargetpg,navitemtagline,navitemdesc,navitemimage];

	var errors = false;

	$.each(itemstovalidate, function(index, value){

		if(value.val() == '')
		{
			errors = true;
			value.siblings('label').children('.requiredmsg').css('display','inline');
		}
		
	});*/

	
	if(errors)
	{
		console.log('Errors found!');
		return false;
	}
	else
	{

		//var file = document.getElementById('navitem-image').files;

		// if action == 'update' && there is some file selected 
		// or if action == 'add'
		if((navform.find('input[name=navitem-action]').val() == 'update' && navform.find('input[name=navitem-image]').val() != '') || navform.find('input[name=navitem-action]').val() == 'add')
		{
			var addfile = true;
			formData.append('imageupadated', 'true');
		}

		if(addfile == true)
		{
			var fileinputelement = $(this).find('input[name=navitem-image]');

			var file = fileinputelement[0].files[0];

			console.log(file);

			// validate file type
			if(!file.type.match('image.*'))
			{
				fileinputelement.siblings('label').children('.requiredmsg').html('Please Select an Image').css('display','inline');
				return false;
			}

			console.log('Good');

			formData.append(fileinputelement.attr('name'), file, file.name);			
		}


		/* debugging - works
		for(var pair of formData.entries()) {
   			console.log(pair[0]+ ', '+ pair[1]); 
		}*/

		//console.log(formData);

		/* this is how you append to formData object
			// Files
			formData.append(name, file, filename);

			// Blobs
			formData.append(name, blob, filename);

			// Strings
			formData.append(name, value); 

		*/

		/*
			$.post(url,data,callback,datatype);
		*/

		//console.log($(this).attr('action'));		// get form's action url

		/* this causes an error (illegal invocation when it tries to serialize data. we have to set content type)
		$.post($(this).attr('action'), formData, function(res){

				console.log(res);

		}, "json");*/

		var urltopost = $(this).attr('action');

		$.ajax({
			method: 'POST',
			url: urltopost,
			data: formData,
			contentType: false,
			dataType: 'json',
			processData: false,

		})
		.done(function(res){
			console.log(res); 
			//response = JSON.parse(res);	// when we set the expected data type to json and when we send with json_encode, no need to parse it with JSON.parse(res)

			//console.log(res.status);
			
			if(res.status == true)
			{
				$.get('http://freudenberg.cms/getnavitemlist.php', { pgid: res.page_id }, function(navitemlist){
					$('.nav-item-list').html(navitemlist);
					//console.log(navitemlist);
				})

				navform.trigger('reset');	// reset form

				// if action == 'update' remove navitem-id hidden element
				if(res.action == 'update')
				{
					// remove navitem-id hidden element
					navform.find('input[name=navitem-id]').remove();
				}

				$('#image-preview').html('Image not selected!');		// reset preview

				$('#myModal').modal('hide');

				//console.log('Success');
			}
		});


	}

})

// handle edit
$('.nav-item-list').on('click', '.editnavitem', function(e){
	e.preventDefault();
	//console.log($(this).attr('data-nav-item-id'));
	console.log('hello click');
	$('#myModal').modal('show');

	$.get('http://freudenberg.cms/getnavitemdetails.php', { pgid: pg }, function(navitemlist){
		//$('.nav-item-list').html(navitemlist);
		console.log(navitemlist);
		itemdata = JSON.parse(navitemlist);		// parse json_encoded response from php. this is required as we didn't tell $.get that we expect to get json
		// {"id":"28","page_id":"20","title":"fdsa","tagline":"fdsffds","description":"fdsa","target_pg":"1","image":"d1d3c92262fc555b604c7ac70b5e0d2f_555007896.png"}

		console.log(itemdata.title);

		// set form data
		$('#frmnavigation input[name=navitem-name]').val(itemdata.title);
		$('#frmnavigation input[name=navitem-tagline]').val(itemdata.tagline);
		$('#frmnavigation textarea[name=navitem-desc]').val(itemdata.description);
		$('#frmnavigation input[name=navitemsave]').val('Update Item!');
		$('#image-preview').html('<img src="http://freudenberg.cms/uploads/'+itemdata.image+'" style="max-width: 100%; max-height: 100%;">');
		$('#frmnavigation select[name=navitem-target]').val(itemdata.target_pg).change();

		// set the action
		$('#frmnavigation input[name=navitem-action]').val("update");

		// set the nav item id
		$('.hiddenstuff').after('<input type="hidden" name="navitem-id" value="'+itemdata.id+'"/>');

	})	

});