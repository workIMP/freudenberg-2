<?php
require '../appdata/cms/bootstrap.php';

	//echo json_encode($_POST);
	//exit;

	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		// posted
		//$_POST['hello'] = 'amila';

		//echo json_encode($_POST);

		switch($_POST['frmaction'])
		{

			case 'update':
				// update image details
				$updres = $db->updateRecord('UPDATE tblslides SET `alt_text` = ?, `desc_text` = ? WHERE id = ?', array($_POST['alttext'], $_POST['desctext'], (int)$_POST['imageid']));

				if($updres)
				{
					echo json_encode(array('action' => 'update', 'status' => true, 'msg' => 'update success'));
				}
				else
				{
					echo json_encode(array('action' => 'update', 'status' => false, 'msg' => print_r($db->getError(), true)));
				}

			break;

			case 'delete':

				// get image details from db where id = $_POST['imageid']
				$imagedata = $db->getRow('SELECT * FROM tblslides WHERE id = ?', array($_POST['imageid']));


				// unlink main image and thumbnail
				if(unlink(APP_ROOT .'/public_html/images/'.$imagedata->section_id.'/'.$imagedata->page_id.'/'.$imagedata->image_name) && unlink(APP_ROOT .'/public_html/images/'.$imagedata->section_id.'/'.$imagedata->page_id.'/thumbs/'.$imagedata->image_name))
				{
					// image delete success -> now delete the db row
					$delres = $db->deleteRecord('DELETE FROM tblslides WHERE id = ?', array($_POST['imageid']));
					
					if($delres)
					{
						echo json_encode(array('action' => 'delete-record', 'status' => true, 'msg' => 'delete success'));
					}
					else
					{
						echo json_encode(array('action' => 'delete-record', 'status' => false, 'msg' => print_r($db->getError(), true)));
					}
				}
				else
				{
					echo json_encode(array('action' => 'delete-file', 'status' => false, 'msg' => APP_ROOT.'\public_html\images\\'.$imagedata->section_id.'\\'.$imagedata->page_id.'\\'.$imagedata->image_name));
				}


			break;

		}
	}
