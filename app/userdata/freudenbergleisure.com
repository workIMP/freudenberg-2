--- 
customlog: 
  - 
    format: combined
    target: /etc/apache2/logs/domlogs/freudenbergleisure.com
  - 
    format: "\"%{%s}t %I .\\n%{%s}t %O .\""
    target: /etc/apache2/logs/domlogs/freudenbergleisure.com-bytes_log
documentroot: /home2/fyfxamvcmvss/public_html
group: fyfxamvcmvss
hascgi: 0
homedir: /home2/fyfxamvcmvss
ifmodulealiasmodule: 
  scriptalias: 
    - 
      path: /home2/fyfxamvcmvss/public_html/cgi-bin/
      url: /cgi-bin/
ifmoduleconcurrentphpc: {}

ifmoduleincludemodule: 
  directoryhomefyfxamvcmvsspublichtml: 
    ssilegacyexprparser: 
      - 
        value: " On"
ifmodulelogconfigmodule: 
  ifmodulelogiomodule: {}

ifmodulemodincludec: 
  directoryhomefyfxamvcmvsspublichtml: 
    ssilegacyexprparser: 
      - 
        value: " On"
ifmodulemodsuphpc: 
  group: fyfxamvcmvss
ifmoduleuserdirmodule: 
  ifmodulempmitkc: 
    ifmoduleruidmodule: {}

ip: 128.199.220.102
owner: root
phpopenbasedirprotect: 1
phpversion: ea-php56
port: 82
scriptalias: 
  - 
    path: /home2/fyfxamvcmvss/public_html/cgi-bin
    url: /cgi-bin/
serveradmin: webmaster@freudenbergleisure.com
serveralias: freudenbergleisure.lk fsaleisure.com fsalk.com mail.freudenbergleisure.com mail.freudenbergleisure.lk mail.fsaleisure.com mail.fsalk.com www.freudenbergleisure.com www.freudenbergleisure.lk www.fsaleisure.com www.fsalk.com
servername: freudenbergleisure.com
ssl: 1
usecanonicalname: 'Off'
user: fyfxamvcmvss
userdirprotect: ''
